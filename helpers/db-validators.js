const News = require("../models/news.model");
const { Product, Category, Kind, Promotion } = require("../models");
const User = require("../models/user.model");

const existEmail = async (email = '') => {
    const existEmail = await User.findOne({email});
    if (existEmail){
        throw new Error(`El correo ${email} ya esta registrado en la base da datos`);
    }
}

const existEmailUpdate = async (email = '', id = '') => {
    const existEmail = await User.findOne({email});
    // const user = await User.findById(id);
    console.log(email + ' El email mandado');
    console.log(id + ' El id mandado');
    console.log(existEmail + ' Usuario encontrado');
    if (existEmail){
        if (existEmail._id != id){
            throw new Error(`El correo ${email} ya esta registrado en la base da datos Actualizado  con el id ${id}`);
        }
    }
}

const existUserForId = async (id) => {
    const existUser = await User.findById(id);
    if (!existUser){
        throw new Error(`El id ${id} no representa a un usuario  en la base da datos`);
    }
}

const allowedCollections = (collection = '', collections = []) => {
    const included = collections.includes(collection);
    if(!included){
        throw new Error(`La collección  ${collection} no esta permita, colecciones validas ${collections} `)
    }
    return true;
}

const existNewsForId = async (id) => {
    const existNews = await News.findById(id);
    if (!existNews){
        throw new Error(`El id ${id} no representa a una noticia  en la base da datos`);
    }
}

const existNewsForSlug = async (slug) => {
    const existNews = await News.findOne({slug});
    if (!existNews){
        throw new Error(`El slug ${slug} no representa a una noticia  en la base da datos`);
    }
}

const existNewsSlug = async (slug) => {
    const existNews = await News.findOne({slug});
    if (existNews){
        throw new Error(`El slug ${slug} no se puede repetir cambiar el titulo de la noticia`);
    }
}

const existPromotionForId = async (id) => {
    const existPromotion = await Promotion.findById(id);
    if (!existPromotion){
        throw new Error(`El id ${id} no representa a una promoción  en la base da datos`);
    }
}


const existCategoryForSlug = async (slug) => {
    const existCategory = await Category.findOne({slug});
    if (!existCategory){
        throw new Error(`El slug ${slug} no representa a una categoria  en la base da datos`);
    }
}

const existCategorySlug = async (slug) => {
    const existCategory = await Category.findOne({slug});
    if (existCategory){
        throw new Error(`El slug ${slug} no se puede repetir cambiar el nombre de la noticia`);
    }
}

const existCategoryForId = async (id) => {
    const existCategory = await Category.findById(id);
    if (!existCategory){
        throw new Error(`El id ${id} no representa a una categoria  en la base da datos`);
    }
}

const existProductForSlug = async (slug) => {
    const existProduct = await Product.findOne({slug});
    if (existProduct){
        throw new Error(`El slug ${slug} no se puede repetir cambiar el nombre del producto`);
    }
}

const existProductForId = async (id) => {
    const existProduct = await Product.findById(id);
    if (!existProduct){
        throw new Error(`El id ${id} no representa a un producto  en la base da datos`);
    }
}

const existKindForId = async (id) => {
    const existKind = await Kind.findById(id);
    if (!existKind){
        throw new Error(`El id ${id} no representa a una especie  en la base da datos`);
    }
}


module.exports = {
    existEmail,
    existEmailUpdate,
    existUserForId,
    existNewsForSlug,
    existNewsForId,
    existNewsSlug,
    existKindForId,
    existPromotionForId,
    allowedCollections,
    existCategoryForSlug,
    existCategoryForId,
    existCategorySlug,
    existProductForId,
    existProductForSlug
}
