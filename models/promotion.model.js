const { Schema, model } = require('mongoose');

const PromotionSchema =  Schema({
    name: {
        type: String,
        required: [true, 'El nombre es obligatorio']
    },
    title: {
        type: String,
    },
    description: {
        type: String,
    },
    url: {
        type: String,
    },
    image: {
        type: String,
        required: [true, 'La imagen es obligatorio']
    },
    active: {
        type: Boolean,
        default: true
    },
    delete: {
        type: Boolean,
        default: false
    }
});

PromotionSchema.methods.toJSON = function () {
    const { __v, ...model } = this.toObject();;
    return model;
}

module.exports = model('Promotion',  PromotionSchema);
