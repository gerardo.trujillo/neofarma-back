const Category = require('./category.model');
const Kind = require('./kind.model');
const News = require('./news.model');
const Product = require('./product.model');
const Promotion = require('./promotion.model');
const Route = require('./route.model');
const User = require('./user.model');
const Server = require('./server');

module.exports = {
    Category,
    Kind,
    News,
    Product,
    Promotion,
    Route,
    User,
    Server
}
