const { Schema, model } = require('mongoose');

const ProductSchema =  Schema({
    name: {
        type: JSON,
        required: [true, 'El nombre es obligatorio']
    },
    slug: {
        type: String,
        unique: true,
        required: [true, 'El slug es obligatorio']
    },
    formula: {
        type: JSON,
    },
    dose: {
        type: JSON,
    },
    use: {
        type: JSON,
    },
    indications: {
        type: JSON,
    },
    presentations: {
        type: JSON,
    },
    warnings: {
        type: JSON,
    },
    kinds: [{
        type: Schema.Types.ObjectId,
        ref: 'Kind',
    }],
    categories: [{
        type: Schema.Types.ObjectId,
        ref: 'Category',
    }],
    routes: [{
        type: Schema.Types.ObjectId,
        ref: 'Route',
    }],
    characteristics: {
        type: JSON,
    },
    image: {
        type: String,
    },
    file: {
        type: String,
    },
    record: {
        type: String,
    },
    images: [{
        type: String
    }],
    active: {
        type: Boolean,
        default: true
    },
    delete: {
        type: Boolean,
        default: false
    }
});

ProductSchema.methods.toJSON = function () {
    const { __v, ...model } = this.toObject();
    return model;
}

module.exports = model('Product',  ProductSchema);
