const { Schema, model } = require('mongoose');

const CategorySchema =  Schema({
    name: {
        type: JSON,
        required: [true, 'El nombre es obligatorio']
    },
    slug: {
        type: String,
        unique: true,
        required: [true, 'El slug es obligatorio']
    },
    parent_id: {
        type: String,
    },
    content: {
        type: JSON,
    },
    image: {
        type: String,
    },
    images: [{
        type: String
    }],
    active: {
        type: Boolean,
        default: true
    },
    delete: {
        type: Boolean,
        default: false
    }
});

CategorySchema.methods.toJSON = function () {
    const { __v, ...model } = this.toObject();
    return model;
}

module.exports = model('Category',  CategorySchema);
