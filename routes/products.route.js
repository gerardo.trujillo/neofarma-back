const { Router} = require('express');
const { check } = require('express-validator');
const { validateFields, validateJWT } = require('../middlewares');
const { existProductForId, existProductForSlug } = require("../helpers/db-validators");
const { productsGetPublic, productsGet, productShowPublic, productPost,
    productPut, productShow, productActive, productDesActive, productDelete,
    productsChildrenShow, productsCategoryGetPublic } = require("../controllers/products.controller");

const router = Router();

router.get('/public', productsGetPublic);
router.get('/public/:category', productsCategoryGetPublic);

router.get('/public/show/:slug', [
    check('slug', 'No es un id valido').not().isEmpty(),
    validateFields
], productShowPublic);

router.get('/', [
    validateJWT
] , productsGet);

router.post('/', [
    validateJWT,
    check('name', 'El nombre es obligatorio').not().isEmpty(),
    validateFields
], productPost);

router.put('/:id', [
    validateJWT,
    check('id', 'No es un id valido').isMongoId(),
    check('id').custom(existProductForId),
    validateFields
], productPut);

router.get('/:id', [
    validateJWT,
    check('id', 'No es un id valido').isMongoId(),
    check('id').custom(existProductForId),
    validateFields
], productShow);

router.get('/children/:id', [
    validateJWT,
    check('id', 'No es un id valido').isMongoId(),
    check('id').custom(existProductForId),
    validateFields
], productsChildrenShow);

router.get('/active/:id', [
    validateJWT,
    check('id', 'No es un id valido').isMongoId(),
    check('id').custom(existProductForId),
    validateFields
], productActive);

router.get('/des-active/:id', [
    validateJWT,
    check('id', 'No es un id valido').isMongoId(),
    check('id').custom(existProductForId),
    validateFields
], productDesActive);

router.delete('/:id', [
    validateJWT,
    check('id', 'No es un id valido').isMongoId(),
    check('id').custom(existProductForId),
    validateFields
], productDelete);

module.exports = router;
