const { request, response } = require('express');
const { Product, Category } = require('../models');
const urlSlug = require('url-slug');
const { fileUpload: fileUploadHelper } = require("../helpers");

const productsGetPublic = async (req, res = response) => {

    const [ totalProducts, products] = await Promise.all([
        Product.countDocuments({delete:false}),
        Product.find({delete:false}).populate('kinds')
    ]);

    res.json({
        totalProducts,
        products,
    });
};

const productsCategoryGetPublic = async (req, res = response) => {

    const slug = req.params.category;
    const category = await Category.findOne({slug});
    const categoryId = category._id;

    const [ totalProducts, products] = await Promise.all([
        Product.countDocuments({delete:false, categories: [category]}),
        Product.find({delete:false, categories: {$all: [category]} }).populate('kinds')
    ]);

    res.json({
        totalProducts,
        products,
        category
    });
};

const productsGet = async (req, res = response) => {

    const [ total, products ] = await Promise.all([
        Product.countDocuments({delete:false}),
        Product.find({delete:false}).populate('categories', 'name').populate('kinds', 'name').populate('routes', 'name')
    ]);

    res.json({
        total,
        products
    });
};

const productPost = async (req = request, res = response) => {

    let { name, formula, dose, indications, routes, presentations, record, warnings, kinds, categories, language } = req.body;
    const slug = urlSlug(name);
    const existProduct = await Product.findOne({slug});
    if (existProduct){
        return res.status(400).json({
            msg: `El slug ${slug} no se puede repetir cambiar el nombre del producto`
        })
    }
    let image;
    if(req.files){
        image = await fileUploadHelper(req.files, undefined, 'products');
    }
    let file;
    if(req.files){
        file = await fileUploadHelper(req.files, 'pdf', 'products');
    }
    let data = getDataTranslateCreate(language, name, formula, dose, indications, routes, presentations, warnings, kinds, categories, image, record, file);
    const product = new Product(data);

    // Guardar en Base de datos
    await product.save();

    res.json({
        product
    })
}

const productActive = async (req = request, res = response) => {

    const id = req.params.id;

    const [product] = await Promise.all([
        Product.findByIdAndUpdate(id,{active:true}, {new:true})
    ]);

    const authenticatedUser = req.user;

    res.json({
        product,
        authenticatedUser
    });
}

const productDesActive = async (req = request, res = response) => {

    const id = req.params.id;

    const [product] = await Promise.all([
        Product.findByIdAndUpdate(id,{active:false}, {new:true})
    ]);

    const authenticatedUser = req.user;

    res.json({
        product,
        authenticatedUser
    });
}

const productShow = async ( req = request, res = response) => {
    const id = req.params.id;

    const product = await Product.findById(id).populate('categories').populate('kinds').populate('routes');
    res.status(200).json({
        product
    });
}

const productsChildrenShow = async ( req = request, res = response) => {
    const id = req.params.id;

    const products = await Product.find({parent_id:id});
    res.status(200).json({
        products
    });
}

const productShowPublic = async ( req = request, res = response) => {
    const slug = req.params.slug;

    const product = await Product.findOne({slug}).populate('kinds').populate('routes');
    res.status(200).json({
        product
    });
}

const productPut = async ( req = request, res = response) => {
    const id = req.params.id;
    let { name, formula, dose, indications, routes, presentations, warnings, kinds, categories, record, language } = req.body;
    const productDB = await Product.findById(id);
    productDB.routes = [];
    productDB.kinds = [];
    productDB.categories = [];
    await productDB.save();
    let image;
    let existImage = false;
    let existFile = false;
    if(req.files){
        image = await fileUploadHelper(req.files, undefined, 'products');
        existImage = true;
    }
    let file;
    // if(req.files){
    //     file = await fileUploadHelper(req.files, 'pdf', 'products');
    //     existFile = true;
    // }
    let data = getDataTranslateUpdate(language, name, formula, dose, indications, routes, presentations, warnings, kinds, categories, image, productDB, existImage, record, file, existFile);
    const product = await Product.findByIdAndUpdate(id, data, {new: true});
    res.status(201).json({
        product
    });
}

const productDelete = async (req = request, res = response) => {
    const id = req.params.id;

    const [product] = await Promise.all([
        Product.findByIdAndUpdate(id,{delete:true}, {new:true})
    ]);

    const authenticatedUser = req.user;

    res.json({
        product,
        authenticatedUser
    });
}

function getDataTranslateCreate(language, name, formula, dose, indications, routes, presentations, warnings, kinds, categories, image, record, file){
    let data;
    const slug = urlSlug(name);
    let nameComplete = {};
    let formulaComplete = {};
    let doseComplete = {};
    let indicationsComplete = {};
    let presentationsComplete = {};
    let warningsComplete = {};
    if (language == 'es'){
        nameComplete = {
            es: name
        };
        formulaComplete = {
            es: formula
        };
        doseComplete = {
            es: dose
        };
        indicationsComplete = {
            es: indications
        };
        presentationsComplete = {
            es: presentations
        };
        warningsComplete = {
            es: warnings
        };
    } else if(language == 'en'){
        nameComplete = {
            en: name
        };
        formulaComplete = {
            en: formula
        };
        doseComplete = {
            en: dose
        };
        indicationsComplete = {
            en: indications
        };
        presentationsComplete = {
            en: presentations
        };
        warningsComplete = {
            en: warnings
        };
    }
    data = {
        name: nameComplete,
        slug,
        record: record,
        formula: formulaComplete,
        dose: doseComplete,
        indications: indicationsComplete,
        presentations: presentationsComplete,
        warnings: warningsComplete,
        routes: JSON.parse(routes),
        kinds: JSON.parse(kinds),
        categories: JSON.parse(categories),
        image,
        file
    };

    return data;
}


function getDataTranslateUpdate(language, name, formula, dose, indications, routes, presentations, warnings, kinds, categories, image, product, exist, record, file, exitFile){
    let data;
    const slug = urlSlug(name);
    let nameComplete = {};
    let formulaComplete = {};
    let doseComplete = {};
    let indicationsComplete = {};
    let routeOfAdministrationComplete = {};
    let presentationsComplete = {};
    let warningsComplete = {};
    if (language == 'es'){
        if (product.name.es && !product.name.en){
            nameComplete = {
                es: name
            }
        } else {
            nameComplete = {
                es: name,
                en: product.name.en
            }

        }
        if (product.formula){
            if (product.formula.es && !product.formula.en){
                formulaComplete = {
                    es: formula
                }
            } else if (!product.formula.es && !product.formula.en){
                formulaComplete = {
                    es: formula,
                }
            } else {
                formulaComplete = {
                    es: formula,
                    en: product.formula.en
                }
            }
        } else {
            formulaComplete = {
                es: formula
            }
        }
        if (product.dose){
            if (product.dose.es && !product.dose.en){
                doseComplete = {
                    es: dose
                }
            } else if (!product.dose.es && !product.dose.en){
                doseComplete = {
                    es: dose,
                }
            } else {
                doseComplete = {
                    es: dose,
                    en: product.dose.en
                }
            }
        } else {
            doseComplete = {
                es: dose
            }
        }
        if (product.indications){
            if (product.indications.es && !product.indications.en){
                indicationsComplete = {
                    es: indications
                }
            } else if (!product.indications.es && !product.indications.en){
                indicationsComplete = {
                    es: indications,
                }
            } else {
                indicationsComplete = {
                    es: indications,
                    en: product.indications.en
                }
            }
        } else {
            indicationsComplete = {
                es: indications
            }
        }
        if (product.presentations){
            if (product.presentations.es && !product.presentations.en){
                presentationsComplete = {
                    es: presentations
                }
            } else if (!product.presentations.es && !product.presentations.en){
                presentationsComplete = {
                    es: presentations,
                }
            } else {
                presentationsComplete = {
                    es: presentations,
                    en: product.presentations.en
                }
            }
        } else {
            presentationsComplete = {
                es: presentations
            }
        }
        if (product.warnings){
            if (product.warnings.es && !product.warnings.en){
                warningsComplete = {
                    es: warnings
                }
            } else if (!product.warnings.es && !product.warnings.en){
                warningsComplete = {
                    es: warnings,
                }
            } else {
                warningsComplete = {
                    es: warnings,
                    en: product.warnings.en
                }
            }
        } else {
            warningsComplete = {
                es: warnings
            }
        }
    } else if(language == 'en'){
        if (product.name.es && !product.name.en){
            nameComplete = {
                en: name
            }
        } else {
            nameComplete = {
                en: name,
                es: product.name.es
            }

        }
        if (product.formula){
            if (product.formula.es && !product.formula.en){
                formulaComplete = {
                    en: formula
                }
            } else if (!product.formula.es && !product.formula.en){
                formulaComplete = {
                    en: formula,
                }
            } else {
                formulaComplete = {
                    en: formula,
                    es: product.formula.es
                }
            }
        } else {
            formulaComplete = {
                en: formula
            }
        }
        if (product.dose){
            if (product.dose.es && !product.dose.en){
                doseComplete = {
                    en: dose
                }
            } else if (!product.dose.es && !product.dose.en){
                doseComplete = {
                    en: dose,
                }
            } else {
                doseComplete = {
                    en: dose,
                    es: product.dose.es
                }
            }
        } else {
            doseComplete = {
                en: dose
            }
        }
        if (product.indications){
            if (product.indications.es && !product.indications.en){
                indicationsComplete = {
                    en: indications
                }
            } else if (!product.indications.es && !product.indications.en){
                indicationsComplete = {
                    en: indications,
                }
            } else {
                indicationsComplete = {
                    en: indications,
                    es: product.indications.es
                }
            }
        } else {
            indicationsComplete = {
                en: indications
            }
        }
        if (product.presentations){
            if (product.presentations.es && !product.presentations.en){
                presentationsComplete = {
                    en: presentations
                }
            } else if (!product.presentations.es && !product.presentations.en){
                presentationsComplete = {
                    en: presentations,
                }
            } else {
                presentationsComplete = {
                    en: presentations,
                    es: product.presentations.es
                }
            }
        } else {
            presentationsComplete = {
                en: presentations
            }
        }
        if (product.warnings){
            if (product.warnings.es && !product.warnings.en){
                warningsComplete = {
                    en: warnings
                }
            } else if (!product.warnings.es && !product.warnings.en){
                warningsComplete = {
                    en: warnings,
                }
            } else {
                warningsComplete = {
                    en: warnings,
                    es: product.warnings.es
                }
            }
        } else {
            warningsComplete = {
                en: warnings
            }
        }
    }
    if (exist && exitFile){
        data = {
            name: nameComplete,
            slug,
            record: record,
            formula: formulaComplete,
            dose: doseComplete,
            indications: indicationsComplete,
            routeOfAdministration: routeOfAdministrationComplete,
            presentations: presentationsComplete,
            warnings: warningsComplete,
            kinds: JSON.parse(kinds),
            categories: JSON.parse(categories),
            image,
            file
        };
    } else if (exist && !exitFile){
        data = {
            name: nameComplete,
            slug,
            record: record,
            formula: formulaComplete,
            dose: doseComplete,
            indications: indicationsComplete,
            routeOfAdministration: routeOfAdministrationComplete,
            presentations: presentationsComplete,
            warnings: warningsComplete,
            kinds: JSON.parse(kinds),
            categories: JSON.parse(categories),
            image,
        };
    } else if (!exist && exitFile){
        data = {
            name: nameComplete,
            slug,
            record: record,
            formula: formulaComplete,
            dose: doseComplete,
            indications: indicationsComplete,
            routeOfAdministration: routeOfAdministrationComplete,
            presentations: presentationsComplete,
            warnings: warningsComplete,
            kinds: JSON.parse(kinds),
            categories: JSON.parse(categories),
            file
        };
    } else {
        data = {
            name: nameComplete,
            slug,
            record: record,
            formula: formulaComplete,
            dose: doseComplete,
            indications: indicationsComplete,
            presentations: presentationsComplete,
            warnings: warningsComplete,
            routes: JSON.parse(routes),
            kinds: JSON.parse(kinds),
            categories: JSON.parse(categories)
        };
    }

    return data;
}

module.exports = {
    productsGetPublic,
    productsCategoryGetPublic,
    productShowPublic,
    productsGet,
    productPost,
    productActive,
    productDesActive,
    productShow,
    productPut,
    productsChildrenShow,
    productDelete
}
