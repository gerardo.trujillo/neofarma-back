const { request, response } = require('express');
const { Kind, Route} = require('../models');
const urlSlug = require('url-slug');
const { fileUpload: fileUploadHelper } = require("../helpers");

// const categoriesGetPublic = async (req, res = response) => {
//
//     const [ totalCategories, categories] = await Promise.all([
//         Category.countDocuments({delete:false}),
//         Category.find({delete:false})
//     ]);
//
//     res.json({
//         totalCategories,
//         categories,
//     });
// };
//
const routesGet = async (req, res = response) => {

    const [ total, routes ] = await Promise.all([
        Route.countDocuments({delete:false}),
        Route.find({delete:false})
    ]);

    res.json({
        total,
        routes
    });
};

const routePost = async (req = request, res = response) => {

    let { name } = req.body;
    const slug = urlSlug(name);
    let image;
    if(req.files){
        image = await fileUploadHelper(req.files, undefined, 'kinds');
    }
    const route = new Route({name:name, slug, image});
    // Guardar en Base de datos
    await route.save();

    res.json({
        route
    })
}
// const categoryActive = async (req = request, res = response) => {
//
//     const id = req.params.id;
//
//     const [category] = await Promise.all([
//         Category.findByIdAndUpdate(id,{active:true}, {new:true})
//     ]);
//
//     const authenticatedUser = req.user;
//
//     res.json({
//         category,
//         authenticatedUser
//     });
// }
//
// const categoryDesActive = async (req = request, res = response) => {
//
//     const id = req.params.id;
//
//     const [category] = await Promise.all([
//         Category.findByIdAndUpdate(id,{active:false}, {new:true})
//     ]);
//
//     const authenticatedUser = req.user;
//
//     res.json({
//         category,
//         authenticatedUser
//     });
// }
//
// const categoryShow = async ( req = request, res = response) => {
//     const id = req.params.id;
//
//     const category = await Category.findById(id);
//     res.status(200).json({
//         category
//     });
// }
//
// const categoriesChildrenShow = async ( req = request, res = response) => {
//     const id = req.params.id;
//
//     const categories = await Category.find({parent_id:id});
//     res.status(200).json({
//         categories
//     });
// }
//
// const categoryShowPublic = async ( req = request, res = response) => {
//     const slug = req.params.slug;
//
//     const category = await Category.findOne({slug});
//     res.status(200).json({
//         category
//     });
// }
//
// const categoryPut = async ( req = request, res = response) => {
//     const id = req.params.id;
//     let { name, content, parent_id, language } = req.body;
//     const categoryDB = await Category.findById(id);
//     let nameComplete = {};
//     let slugComplete = urlSlug(name);
//     let contentComplete = {};
//     if (!parent_id){
//         parent_id = null;
//     }
//     if (language == 'es'){
//         if (categoryDB.name.es && !categoryDB.name.en){
//             nameComplete = {
//                 es: name
//             }
//         } else {
//             nameComplete = {
//                 es: name,
//                 en: categoryDB.name.en
//             }
//
//         }
//         if (categoryDB.content){
//             if (categoryDB.content.es && !categoryDB.content.en){
//                 contentComplete = {
//                     es: content
//                 }
//             } else if (!categoryDB.content.es && !categoryDB.content.en){
//                 contentComplete = {
//                     es: content,
//                 }
//             } else {
//                 contentComplete = {
//                     es: content,
//                     en: categoryDB.content.en
//                 }
//             }
//         } else {
//             contentComplete = {
//                 es: content
//             }
//         }
//
//     } else if(language == 'en'){
//         slugComplete = urlSlug(categoryDB.name.es)
//         if (categoryDB.name.es && !categoryDB.name.en){
//             nameComplete = {
//                 en: name
//             }
//         } else {
//             nameComplete = {
//                 en: name,
//                 es: categoryDB.name.es
//             }
//
//         }
//         if (categoryDB.content){
//             if (categoryDB.content.es && !categoryDB.content.en){
//                 contentComplete = {
//                     en: content
//                 }
//             } else if (!categoryDB.content.es && !categoryDB.content.en){
//                 contentComplete = {
//                     en: content,
//                 }
//             } else {
//                 contentComplete = {
//                     en: content,
//                     es: categoryDB.content.es
//                 }
//             }
//         } else {
//             contentComplete = {
//                 en: content
//             }
//         }
//     }
//     let image;
//     if(req.files){
//         image = await fileUploadHelper(req.files, undefined, 'categories');
//     }
//     const data = {
//         name: nameComplete,
//         slug: slugComplete,
//         content: contentComplete,
//         parent_id,
//         image
//     }
//
//     const category = await Category.findByIdAndUpdate(id, data, {new: true});
//     res.status(201).json({
//         category
//     });
// }
//
// const categoryDelete = async (req = request, res = response) => {
//     const id = req.params.id;
//
//     const [category] = await Promise.all([
//         Category.findByIdAndUpdate(id,{delete:true}, {new:true})
//     ]);
//
//     const authenticatedUser = req.user;
//
//     res.json({
//         category,
//         authenticatedUser
//     });
// }

module.exports = {
    routesGet,
    routePost
}
