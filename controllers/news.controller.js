const { request, response } = require('express');
const { News } = require('../models');
const urlSlug = require('url-slug');
const { fileUpload: fileUploadHelper } = require("../helpers");

const newsGetPublic = async (req, res = response) => {

    const { limit = 3, page = 1 } = req.query;
    let from = (page - 1) * limit;

    const [ totalNews, news ] = await Promise.all([
        News.countDocuments({delete:false}),
        News.find({delete:false})
            .limit(Number(limit))
            .skip(Number(from)),
    ]);

    res.json({
        totalNews,
        news,
    });
};

const newsGetAllPublic = async (req, res = response) => {


    const [ totalNews, news ] = await Promise.all([
        News.countDocuments({delete:false}),
        News.find({delete:false})
    ]);

    res.json({
        totalNews,
        news,
    });
};

const newsGet = async (req, res = response) => {

    const [ total, news ] = await Promise.all([
        News.countDocuments({delete:false}),
        News.find({delete:false})
    ]);

    res.json({
        total,
        news
    });
};

const newsPost = async (req = request, res = response) => {

    const { title, content, intro, date, url, language } = req.body;
    const slug = urlSlug(title);
    const existNews = await News.findOne({slug});
    if (existNews){
        return res.status(400).json({
            msg: `El slug ${slug} no se puede repetir cambiar el titulo de la noticia`
        })
    }
    let image;
    if(req.files){
        image = await fileUploadHelper(req.files, undefined, 'news');
    }
    const data = getDataTranslateCreate(language, title, content, intro, url, date, image);

    const news = new News(data);

    // Guardar en Base de datos
    await news.save();

    res.json({
        news
    })
}

const newsActive = async (req = request, res = response) => {

    const id = req.params.id;

    const [news] = await Promise.all([
        News.findByIdAndUpdate(id,{active:true}, {new:true})
    ]);

    const authenticatedUser = req.user;

    res.json({
        news,
        authenticatedUser
    });
}

const newsDesActive = async (req = request, res = response) => {

    const id = req.params.id;

    const [news] = await Promise.all([
        News.findByIdAndUpdate(id,{active:false}, {new:true})
    ]);

    const authenticatedUser = req.user;

    res.json({
        news,
        authenticatedUser
    });
}

const newsShow = async ( req = request, res = response) => {
    const id = req.params.id;

    const news = await News.findById(id);
    res.status(200).json({
        news
    });
}

const newsShowPublic = async ( req = request, res = response) => {
    const slug = req.params.slug;

    const news = await News.findOne({slug});
    res.status(200).json({
        news
    });
}

const newsPut = async ( req = request, res = response) => {
    const id = req.params.id;
    const { title, content, intro, date, url, language } = req.body;
    const newsDB = await News.findById(id);
    let image;
    let existImage = false;
    if(req.files){
        image = await fileUploadHelper(req.files, undefined, 'news');
        existImage = true;
    }
    const data = getDataTranslateUpdate(language, title, content, intro, url, date, image, existImage, newsDB)
    const news = await News.findByIdAndUpdate(id, data, {new: true});
    res.status(201).json({
        news
    });
}

const newsDelete = async (req = request, res = response) => {
    const id = req.params.id;

    const [news] = await Promise.all([
        News.findByIdAndUpdate(id,{delete:true}, {new:true})
    ]);

    const authenticatedUser = req.user;

    res.json({
        news,
        authenticatedUser
    });
}


function getDataTranslateCreate(language, title, content, intro, url, date, image){
    let data;
    const slug = urlSlug(title);
    let titleComplete = {};
    let contentComplete = {};
    let introComplete = {};
    if (language == 'es'){
        titleComplete = {
            es: title
        };
        contentComplete = {
            es: content
        };
        introComplete = {
            es: intro
        };
    } else if(language == 'en'){
        titleComplete = {
            en: title
        };
        contentComplete = {
            en: content
        };
        introComplete = {
            en: intro
        };

    }
    data = {
        title: titleComplete,
        url,
        slug,
        content: contentComplete,
        intro: introComplete,
        date,
        image
    };

    return data;
}

function getDataTranslateUpdate(language, title, content, intro, url, date, image, exists, news){
    const slug = urlSlug(title);
    let titleComplete = {};
    let contentComplete = {};
    let introComplete = {};
    if (language == 'es'){
        if (news.title.es && !news.title.en){
            titleComplete = {
                es: title
            }
        } else {
            titleComplete = {
                es: title,
                en: news.title.en
            }

        }
        if (news.content){
            if (news.content.es && !news.content.en){
                contentComplete = {
                    es: content
                }
            } else if (!news.content.es && !news.content.en){
                contentComplete = {
                    es: content,
                }
            } else {
                contentComplete = {
                    es: content,
                    en: news.content.en
                }
            }
        } else {
            contentComplete = {
                es: content
            }
        }
        if (news.intro){
            if (news.intro.es && !news.intro.en){
                introComplete = {
                    es: intro
                }
            } else if (!news.intro.es && !news.intro.en){
                introComplete = {
                    es: intro,
                }
            } else {
                introComplete = {
                    es: intro,
                    en: news.intro.en
                }
            }
        } else {
            introComplete = {
                es: intro
            }
        }
    } else if(language == 'en'){
        if (news.title.es && !news.title.en){
            titleComplete = {
                en: title
            }
        } else {
            titleComplete = {
                en: title,
                es: news.title.es
            }

        }
        if (news.content){
            if (news.content.es && !news.content.en){
                contentComplete = {
                    en: content
                }
            } else if (!news.content.es && !news.content.en){
                contentComplete = {
                    en: content,
                }
            } else {
                contentComplete = {
                    en: content,
                    es: news.content.es
                }
            }
        } else {
            contentComplete = {
                en: content
            }
        }
        if (news.intro){
            if (news.intro.es && !news.intro.en){
                introComplete = {
                    en: intro
                }
            } else if (!news.intro.es && !news.intro.en){
                introComplete = {
                    en: intro,
                }
            } else {
                introComplete = {
                    en: intro,
                    es: news.intro.es
                }
            }
        } else {
            introComplete = {
                en: intro
            }
        }
    }
    if (exists){
        data = {
            title: titleComplete,
            url,
            slug,
            content: contentComplete,
            intro: introComplete,
            date,
            file
        };
    } else {
        data = {
            title: titleComplete,
            url,
            slug,
            content: contentComplete,
            intro: introComplete,
            date
        };
    }

    return data;
}

module.exports = {
    newsGetPublic,
    newsGetAllPublic,
    newsShowPublic,
    newsGet,
    newsPost,
    newsActive,
    newsDesActive,
    newsShow,
    newsPut,
    newsDelete
}
