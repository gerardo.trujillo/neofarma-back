const { request, response } = require('express');
const { Category } = require('../models');
const urlSlug = require('url-slug');
const { fileUpload: fileUploadHelper } = require("../helpers");

const categoriesGetPublic = async (req, res = response) => {

    const [ totalCategories, categories] = await Promise.all([
        Category.countDocuments({delete:false}),
        Category.find({delete:false})
    ]);

    res.json({
        totalCategories,
        categories,
    });
};

const categoriesGet = async (req, res = response) => {

    const [ total, categories ] = await Promise.all([
        Category.countDocuments({delete:false}),
        Category.find({delete:false})
    ]);

    res.json({
        total,
        categories
    });
};

const categoryPost = async (req = request, res = response) => {

    let { name, content, parent_id, language } = req.body;
    const slug = urlSlug(name);
    const existCategory = await Category.findOne({slug});
    if (existCategory){
        return res.status(400).json({
            msg: `El slug ${slug} no se puede repetir cambiar el nombre de la categoria`
        })
    }
    let nameComplete = {};
    let contentComplete = {};
    if (language == 'es'){
        nameComplete = {
            es: name
        }
        contentComplete = {
            es: content
        }
    } else if(language == 'en'){
        nameComplete = {
            en: name
        }
        contentComplete = {
            en: content
        }
    }
    if (!parent_id){
        parent_id = null;
    }
    let image;
    if(req.files){
        image = await fileUploadHelper(req.files, undefined, 'categories');
    }
    const category = new Category({name:nameComplete, slug, parent_id, content:contentComplete, image});



    // Guardar en Base de datos
    await category.save();

    res.json({
        category
    })
}

const categoryActive = async (req = request, res = response) => {

    const id = req.params.id;

    const [category] = await Promise.all([
        Category.findByIdAndUpdate(id,{active:true}, {new:true})
    ]);

    const authenticatedUser = req.user;

    res.json({
        category,
        authenticatedUser
    });
}

const categoryDesActive = async (req = request, res = response) => {

    const id = req.params.id;

    const [category] = await Promise.all([
        Category.findByIdAndUpdate(id,{active:false}, {new:true})
    ]);

    const authenticatedUser = req.user;

    res.json({
        category,
        authenticatedUser
    });
}

const categoryShow = async ( req = request, res = response) => {
    const id = req.params.id;

    const category = await Category.findById(id);
    res.status(200).json({
        category
    });
}

const categoriesChildrenShow = async ( req = request, res = response) => {
    const id = req.params.id;

    const categories = await Category.find({parent_id:id, delete:false});
    res.status(200).json({
        categories
    });
}

const categoryShowPublic = async ( req = request, res = response) => {
    const slug = req.params.slug;

    const category = await Category.findOne({slug});
    res.status(200).json({
        category
    });
}

const categoryPut = async ( req = request, res = response) => {
    const id = req.params.id;
    let { name, content, parent_id, language } = req.body;
    const categoryDB = await Category.findById(id);
    let nameComplete = {};
    let slugComplete = urlSlug(name);
    let contentComplete = {};
    if (!parent_id){
        parent_id = null;
    }
    if (language == 'es'){
        if (categoryDB.name.es && !categoryDB.name.en){
            nameComplete = {
                es: name
            }
        } else {
            nameComplete = {
                es: name,
                en: categoryDB.name.en
            }

        }
        if (categoryDB.content){
            if (categoryDB.content.es && !categoryDB.content.en){
                contentComplete = {
                    es: content
                }
            } else if (!categoryDB.content.es && !categoryDB.content.en){
                contentComplete = {
                    es: content,
                }
            } else {
                contentComplete = {
                    es: content,
                    en: categoryDB.content.en
                }
            }
        } else {
            contentComplete = {
                es: content
            }
        }

    } else if(language == 'en'){
        slugComplete = urlSlug(categoryDB.name.es)
        if (categoryDB.name.es && !categoryDB.name.en){
            nameComplete = {
                en: name
            }
        } else {
            nameComplete = {
                en: name,
                es: categoryDB.name.es
            }

        }
        if (categoryDB.content){
            if (categoryDB.content.es && !categoryDB.content.en){
                contentComplete = {
                    en: content
                }
            } else if (!categoryDB.content.es && !categoryDB.content.en){
                contentComplete = {
                    en: content,
                }
            } else {
                contentComplete = {
                    en: content,
                    es: categoryDB.content.es
                }
            }
        } else {
            contentComplete = {
                en: content
            }
        }
    }
    let image;
    let data;
    if(req.files){
        image = await fileUploadHelper(req.files, undefined, 'categories');
        data = {
            name: nameComplete,
            slug: slugComplete,
            content: contentComplete,
            parent_id,
            image
        }
    } else {
        data = {
            name: nameComplete,
            slug: slugComplete,
            content: contentComplete,
            parent_id
        }
    }

    const category = await Category.findByIdAndUpdate(id, data, {new: true});
    res.status(201).json({
        category
    });
}

const categoryDelete = async (req = request, res = response) => {
    const id = req.params.id;

    const [category] = await Promise.all([
        Category.findByIdAndUpdate(id,{delete:true}, {new:true})
    ]);

    const authenticatedUser = req.user;

    res.json({
        category,
        authenticatedUser
    });
}

module.exports = {
    categoriesGetPublic,
    categoryShowPublic,
    categoriesGet,
    categoryPost,
    categoryActive,
    categoryDesActive,
    categoryShow,
    categoryPut,
    categoriesChildrenShow,
    categoryDelete
}
